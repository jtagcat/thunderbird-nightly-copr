AutoReqProv: no

##Init variables

%global packver 92.0a1
%global _optdir /opt
%global arch x86_64

##Package Version and Licences

Summary: Thunderbird Nightly RPM Builds
Name: thunderbird-nightly
Version: %{packver}
Release: 0a1_%(date +%%y%%m%%d)%{?dist}
License: MPLv1.1 or GPLv2+ or LGPLv2+
Source0: https://ftp.mozilla.org/pub/thunderbird/nightly/latest-comm-central-l10n/thunderbird-%{packver}.en-US.linux-%{arch}.tar.bz2
Source1: thunderbird.desktop
Source2: default-policies.json
Group: Applications/Internet
URL: https://www.thunderbird.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

Requires: alsa-lib libX11 libXcomposite libXdamage libnotify libXt libXext glib2 dbus-glib libjpeg-turbo cairo-gobject libffi fontconfig freetype libgcc gtk3 gtk2 hunspell zlib
Requires: nspr >= 4.10.8
Requires: nss >= 3.19.2
Requires: sqlite >= 3.8.10.2

##Description for Package

%description
This package is a package built directly from Mozilla's nightly tarball. This package will be updated weekly if not sooner.

%prep
%setup -q -n thunderbird

## Install Instructions

%install

## Thunderbird explicitly supports python3, so its also our choice of interpreter. Note that there is no dependency on python.
## These scripts are only used when generating debug information in case of a crash, so not critical to the operation of thunderbird.
find %{_builddir} -name '*.py' -type f -exec sed -i -e 's,#!/usr/bin/python,#!/usr/bin/python3,' -e 's,/usr/bin/env python,/usr/bin/env python3,' -s {} \;

install -dm 755 %{buildroot}/usr/{bin,share/{applications,icons/hicolor/128x128/apps},opt}
install -dm 755 %{buildroot}/%{_optdir}/thunderbird-nightly/browser/defaults/preferences/

install -m644 %{_builddir}/thunderbird/chrome/icons/default/default128.png %{buildroot}/usr/share/icons/hicolor/128x128/apps/thunderbird-nightly.png

cp -rf %{_builddir}/thunderbird/* %{buildroot}/opt/thunderbird-nightly/
ln -s /opt/thunderbird-nightly/thunderbird %{buildroot}/usr/bin/thunderbird-nightly

%{__cp} -p %{SOURCE1} %{buildroot}/%{_datadir}/applications/thunderbird.desktop

## Disable Update Alert
%{__mkdir_p} %{buildroot}/%{_optdir}/thunderbird-nightly/distribution
%{__cp} -p %{SOURCE2} %{buildroot}/%{_optdir}/thunderbird-nightly/distribution/policies.json

##Cleanup

%clean
rm -rf $RPM_BUILD_ROOT

##Installed Files


%files
%{_bindir}/%{name}
%{_datadir}/applications/thunderbird.desktop
%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
%{_optdir}/thunderbird-nightly/
